---
name: Bug report
about: graphiqal bug report
title: "[BUG]"
labels: bug
assignees: byeonguk-Yu, ThisIsIsaac

---

## Description
A clear and concise description of what the bug is.

## How to reproduce
Steps to reproduce the behavior:
1. Go to '...'
2. Click on '....'
3. Scroll down to '....'
4. See error

## Expected behavior
A clear and concise description of what you expected to happen.

## Error message
copy & paste full error message or share screenshot

## Environment (please complete the following information):
 - OS: [e.g. CentOS7]
 - Engine: [e.g. Tacotron]
 - Engine github URL: [e.g. github.com/mindslab-ai/graphiqal]
 - graphiqal Version: [e.g. 1.0]

## Additional context
Add any other context about the problem here.
