---
name: Engine packaging
about: Request to package engine
title: "[PACKAGING]"
labels: engine packaging
assignees: byeonguk-Yu, ThisIsIsaac

---

## Checklist

- [ ] Do you have your engine / model wrapped in `class` that follows the [guide](https://github.com/mindslab-ai/graphiqal/blob/master/docs/wrapping_model_guide.md)? If not, **do it BEFORE publishing this issue**.
- [ ] Do you have build shell? If not, **add `needs: buildshell` tag and assign `손동원` to `Assignees`**
- [ ] Do you have test code that follows the [guide](https://github.com/mindslab-ai/graphiqal/blob/master/docs/test_code_guide.md)? If not, **add `needs: test code` tag, add yourself to `Assignees`, and add test code later**
- [ ] Do you have throughput benchmark code that follows the [guide](https://github.com/mindslab-ai/graphiqal/blob/master/docs/benchmark_code_guide.md)? If not, **add `needs: throughput benchmark code` tag, add yourself to `Assignees`, and add benchmark code later**


## Engine information
* **Engine name:**
* **Engine Github URL:**

## Software / hardware requirements
* **OS:** [ex. CentOS7]
* **Python version (MUST be >= 3.5):**
* **Pytorch / Tensorflow version:**
* **minimum GPU spec:** [e.g. GTX 1080 / sm_61 / G
PU DRAM >= 8GB]

## Model file:
* **Class path:**
* **Checkpoint / saved model file path:**

## Test code:

- [ ] Do you have **at least 3** test cases?

* **Engine throughput:**
* **Test file path:**
* **test input file path:**
* **test output file path:**
* **Results reproducibility:** [ **yes** if the output is exactly the same for every run given the same input, **no** otherwise]
* **How to run test:**
1. This is an example. 
1. go to `path/to/test_code.py`
1. `workon venv_demo`
1. `pytest`


## Throughput benchmark code

- [ ] Do you have **at least 3** test cases?
- [ ] Did you **configure `iterations` and `rounds`** to best measure your engine's throughput?

* **throughput measuring file path:**

* **How to measure throughput:**
1. This is an example
1. go to `path/to/benchmark_code.py`
1. `workon venv_demo`
1. `python benchmark_code.py`
