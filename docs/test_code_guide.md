# Test code guide

we are use [pytest](https://pypi.org/project/pytest/) testing framework. pytest performs all tests in serial.

## Test file namging convention

* test code **MUST be in its own seperate file**
* name of file **MUST start or endwith `test`** ( ex. `test_tacotron.py` or `tacotron_test.py`) due to [Pytest's discovery method](https://docs.pytest.org/en/latest/goodpractices.html#test-discovery)

## test function

### test function naming convention

The name of the test function **MUST start with `test`** (ex. `test_tacotron()`) due to [Pytest's discovery method](https://docs.pytest.org/en/latest/goodpractices.html#test-discovery)

### how to write test code

> **MUST have at least 3 different test functions**

> **MUST use the same test cases ( same input & output ) used in benchmarking code**

> **DO NOT call any function or write any code, other than `import` and your 3 test functions**

1. put your test code in 3 different function. DO NOT run any code, simply define 3 test functions and perform test inside the test function with `assert(bool)`.
1. your test function **MUST take no argument**
1. Output **MUST be reproducible**. If it isn't, turn on reproducability ([pytorch guide](https://pytorch.org/docs/stable/notes/randomness.html), [tensorflow reproducability](https://www.tensorflow.org/api_docs/python/tf/random/set_random_seed))
1. import or create 3 inputs and reference outputs ( what you expect to be the correct output of your model )
1. create **AT LEAST 3 test cases** using the 3 pairs of input and output

```python
# content of "benchmark_tacotron.py"

From tacotron_class import Tacotron

# initialize model
tacotorn = Tacotron(arg)

############### test case 1 ###############
# takes no argument
def test_short_text():

    # set input
    text = "hello world"
    speaker_id = 0
    
    # set reference output
    ref_mel = open("path/to/reference_output1", 'r').read()
    
    # run model
    my_mel = tacotron.GetMel(text, speaker_id)
    
    # make assertion to check that output is equal to reference 
    assert(is_mel_equal(my_mel, ref_mel))
    
    
############### test case 2 ###############
def test_long_text(benchmark):

    text = "this is a really really long text because we have to test for various inputs with different\
            characteritics in order to thoroughly test for all edge cases."
    speaker_id = 0

    ref_mel = open("path/to/reference_output2", 'r').read()
    
    my_mel = tacotron.GetMel(text, speaker_id)
    
    assert(is_mel_equal(my_mel, ref_mel))

############### test case 3 ###############
def test_long_text_different_speaker(benchmark):

    text = "this is a really really long text because we have to test for various inputs with different\
            characteritics in order to thoroughly test for all edge cases."
    speaker_id = 1

    ref_mel = open("path/to/reference_output1", 'r').read()
    
    my_mel = tacotron.GetMel(text, speaker_id)
    
    assert(is_mel_equal(my_mel, ref_mel))
```

## Running test

> check that your test runs properly before submitting

1. `pip install pytest`
1. go to directory with your benchmark code
1. in terminal, run: `pytest`
