# How to wrap your DNN model in class

## 1. Define `__init__` method

**Your `__init__` must take at least 2 arguments:**
* `checkpoint_path(str)`: the path to checkpoint file you will load
* `device(int)`: the GPU to load the model. **NOTE: the name of the argument MUST be `device`**


### IMPORTANT rules
1. you can take any number of arguments, but it **MUST include the above 2 arguments**.
1. the name of the argument **MUST be `device`**
1. If you need to open fils or other resources, make sure to open/allocate and close/free the resources within your `__init__`. For example, if you need to open config files, take path to config file as argument and open it inside your `__init__`.
1. Input & output **MUST be Python's built-in type** (`list`, nested `list`, `str`, `int`, `float`, ...) and NOT Pytorch / Tensorflow / Numpy's tensor/types. [More built-in types](https://docs.python.org/3/library/stdtypes.html)

```python
class Tacotron:
    def __init__(self, checkpoint_path, device, threshold):
        # load model in `checkpoint_path` to the specified `device`
        # if you need to open any config files, DO IT INSIDE HERE
```

## 2. Define inference method

1. your inference method can take whatever argument it needs
1. it **MUST `return` the result (`yield` if streaming)**
1. Input & output **MUST be Python's built-in type** (`list`, nested `list`, `str`, `int`, `float`, ...), NOT Pytorch / Tensorflow / Numpy's tensor. [More built-in types](https://docs.python.org/3/library/stdtypes.html)
 
```python
    # take whatever argument it needs
    def GetMel(self, text, speaker):
        # perform inference and create `mel`
        return mel
```


