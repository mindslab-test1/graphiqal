

## How to use pipelining:
```python
from dramatiq import pipeline
import actor

pipe = pipeline([actor.func1.message(), actor.func2.message()])

result_message = pipe.run()
result = result_message.get_result(block=True)
```

when using pipeline, make sure to create a new instance of pipeline everytime you call a new function. Else this can cause undefined behavior, as the same message instances are enqueued and `message_key` is no longer globally unique ( and `message_key` is used to uniquely access result backend in Redis)

```python

pipe_results=[]

# like this
for _ in range(num_run):
    pipe_results.append(pipeline([actor.func1.message(), actor.func2.message()]).run())
```

**pipelineing with streaming:**

you can only stream from the last function. So if there are 2 chains s.t. `func1 -> func2`, only `func2` can be streaming. `func1` will return value as non-stremaing.

1. when creating `pipeline` instance, specify `is_streaming=True`
2. specify `is_streaming=True` in `get_result()`

```python

# when creating `pipeline` instance, specify `is_streaming=True`
pipe = pipeline([actor.func1.message(), actor.func2.message(), is_streaming=True])

result_message = pipe.run()

for result in result_message.get_result(is_streaming=True, block=True):
    #do something with streamed result
    print(result)
```

## How to use Taoctron-Wavenet TTS:
1. run command: `workon venv_tts`
2. make sure no other instance of `dramatiq` is running using `htop`
2. make sure `redis` and `rabbitmq` are already running. If not, then open each with: `sudo systemctl start redis` and `sudo systemctl start rabbitmq-server` respectively
2. under `dramatiq/server_code`, run command: `dramatiq -p n -t 1 tts_server` where `n = number_of_GPU * 2`. `-p` specifies the number of processes and `-t` specifies the number of threads per process. For GPU tasks, multithreading may or may not work (this depends on how the GPU kernel was implemented ). **When using TTS, for the best thrhoughput, make the number of processes 2 times the number of totla GPUs you want to use and number of threads is 1**. You may get lower TPS or GPU RAM overflow when using other parameters for number of threads and processes ( this is so that at least 2 tacotrons are run on every GPU and the `nvidia-smi` utilization reaches 100% )
2. test by opening up a new terminal, running `workon venv_tts` then run `python tts_request.py`. There audio should have been created inside `wav` directory about Trump. 
2. when done running `dramatiq`, you can stop Redis and RabbitMQ with:`sudo systemctrl stop redis` and `sudo systemctrl stop rabbitmq-server`


