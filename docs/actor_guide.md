# Defining actor

**Actor** is a function that is registered in the server.

### Defining CPU actor

This is an instruction to define **actors that only use CPUs**

1. define a regular python function
1. use decorator `@dramatiq.actor(time_limit=float("inf"))` ( `time_limit=float("inf")` argument prevents the server from shutting down automatically after `time_limit` exceeds )
1. if it `return`s or `yield`s, set `store_results=True` 
1. if the functoin will `yield` or return an `Iterable` object ( i.e. has streaming functionality ), set `is_streaming=True`

```python
# this does not save anything 
@dramatiq.actor(time_limit=float("inf"))
def add_and_print_actor(x, y):
    print(x+y)

# this saves (x+y) in Redis
@dramatiq.actor(time_limit=float("inf"), store_results=True)
def add_and_save_actor(x, y):
    return x+y
```
---

### Defining GPU actor

This is an instruction to define **actors that use GPUs**

1. package the GPU model you will run ([more on packaging your DNN model](https://github.com/mindslab-ai/graphiqal/blob/master/README.md#wrap-your-dnn-model-in-a-class))
1. define an actor the same way you did for CPU ([more on defining CPU actor](https://github.com/mindslab-ai/graphiqal/blob/master/README.md#wrap-your-dnn-model-in-a-class))
1. add `model` as a keyword-only argument to the functio you have defined
1. call `model`'s inference method you have defined

```python
# non-streaming example that saves melspectrogram in Redis
@dramatiq.actor(time_limit=float("inf"), store_results=True)
def tacotron_actor(text, speaker, *, model):

    # call whatever method you have defined to run inference
    # the method defined in Tacotron is GetMel(text, speaker)
    return model.GetMel(text, speaker)

# streaming example that saves audio in Redis
# in order to stream DNN models, the method that performs inference must return an iterable object
@dramatiq.actor(time_limit=float("inf"), store_results=True, is_streaming=True)
def wavenet_actor(mel):
    
    # Wavenet.GetWavData(mel) must also return an iterable object
    for audio in model.GetWavData(mel):
        yield audio
```

take a look at [tts_actor.py](https://github.com/mindslab-ai/graphiqal/blob/master/examples/tts_example/tts_actor.py) for more detailed example
