# Sending requests to server

## 1. Import server

Import actors you have defined.

```python
from tts_server import tacotron_actor, wavenet_actor
```

## 2. send request to server

use `send` to send request to the server and pass arguments for the actor to `send`.

```python
sentence = "hello world"
speaker_id = 0

# send request to the server ( by enqueuing request to RabbitMQ )
tacotron_result = tacotron_actor.send(sentence, speaker_id)
```
---

**If your actor is streaming**, then you must:
1. use `send_with_options` instead of `send`
1. specify `options` arguement as a dictionary containing `"is_streaming":True`
```python
# specify options
wavenet_result = wavenet_actor.stream_func.send_with_options(args=(mel), options={"is_streaming":True})
```

## 3. Retrieve results

Arguments:
* `block(bool)` - whether to wait for the result or not. If `False` and result is not produced by the time of calling the function, raises `dramatiq.results.errors.ResultMissing` exception.
* `timeout(int)` - number of miliseconds to wait for the result. If the result is not produced after waiting for `timieout` miliseconds, raises `dramatiq.results.errors.ResultTimeout` exception. If `block=False`, then `timeout` is disregarded.

```python
# wait 1 second for the result to be produced.
mel = tacotron_result.get_result(block=True, timeout=1000)
```
---

**If your actor is streaming**, then you must:
1. specify `is_streaming=True` in `get_result`
1. use the returned value of `get_result` as an `Iterator`

```python
# specify is_streaming and block
for result in result_message.get_result(is_streaming=True, block=True, timeout = 1000):
    print(result) # do something with result
```
---

# Trouble Shooting when testing your server

## Resetting RabbitMQ
requests are enqueued in a queue with the same name as your actor's name. When a message fails, it is automatically saved in `actor_name.xq`. Even if you kill the server and RabbitMQ, the items in the queues are preserved. When the server boots back up, it will move all items from `actor_name.xq` to `actor_name`, which means the same message is going to be processed. If the message is inherently causing problems, then try to reset RabbitMQ.

**How to reset RabbitMQ queues:**
1. make sure the server is not alive
1. `sudo rabbitmqctl stop_app`
1. `sudo rabbitmqctl reset`
1. `sudo systemctl stop rabbitmq-server`
1. `sudo systemctl start rabbitmq-server`
---

## Resetting Redis
Very rarely will you ever have to reset Redis. If you think there is a problem with the Redis backend, inspect it with `redis-cli`. 

**How to reset Redis:**
1. make sure the server is not alive
1. `sudo redis-cli flushall`
