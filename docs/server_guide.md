# Quick start guide: defining your server
In this guide, we will explain how to set up your DNN model server. We are going to use [examples/tts_example/tts_server.py](https://github.com/mindslab-ai/graphiqal/blob/master/examples/tts_example/tts_server.py) as an example to step through each relevant part.

## 1. Import and setup server

the below imports are necessary whenever you want to create a server.

Simply **copy and paste**.

```python
######################################## COPY & PASTE ########################################
# necessary imports for the server
import dramatiq
import sys
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from dramatiq.results.backends.redis import RedisBackend
from dramatiq.results import Results
from dramatiq.middleware.worker_middleware import WorkerMiddleware, Model, WorkerConfig

# setup server
broker = RabbitmqBroker()
redis_client = RedisBackend()
broker.add_middleware(Results(backend=redis_client))
dramatiq.set_broker(broker)
##############################################################################################
```

## 2. Import your DNN class

if you are defining DNN model server, you must also import classes you have defined that wraps your model ( if you haven't yet wrapped your DNN model in a class, [click here]() to read how to). If the classes are saved outside of the `graphiqal` directory, then you need to add it to the search tree with `sys.path.insert(0, path)` where `path` is the path to your python file that defines the class.

Here is an example in [tts_server.py](https://github.com/mindslab-ai/graphiqal/blob/master/examples/tts_example/tts_server.py). Here, we are importing two classes: `Tacotron` saved in `tacotron_class.py` and `Wavenet` saved in `wavenet_class.py`.

The full paths are:
* `/home/minds/git/brain-tts/pysrc/maum/brain/tts/tacotron2_old/run/tacotron_class.py`
* `/home/minds/git/brain-tts/pysrc/maum/brain/tts/wavenet/run/wavenet_class.py`

```python
# add path to the search tree
sys.path.insert(0, '/home/minds/git/brain-tts/pysrc')

# perform necessary imports
from maum.brain.tts.tacotron2_old.run.tacotron_class import Tacotron
from maum.brain.tts.wavenet.run.wavenet_class import WaveNet
```

## 3. Import defined actor

Import actors
```python
from tts_actor import tacotron_actor, wavenet_actor
```

if you haven't defined actors yet, take a look at this [guide for writing actors](https://github.com/mindslab-ai/graphiqal/blob/master/docs/actor_guide.md)


## 4. configure Model and Worker
### `Class Model`
represents a single algorithm. It takes 3 manditory positional arguements: `model_name`, `model`, and `kwargs`. Create an instance for each algorithm you will use.


* `model_name (str)` - name of the model that will be used as a key into the dictionary which will store all the modesl of the worker
* `model (Model)` - the class of the model that must have `__init__` defined. The model's `__init__` **MUST** take `device` as an arguement which specifies which device the model will be loaded to.
* `kwargs (dict) ` - key word arguements that `model`'s `__init__` will take. `device` arguement given here will be overwritten by `WorkerConfig.device`

```python
# set path to the models we will import
tacotron_eng_model_path = "/home/minds/maum/trained/tts/tacotron2/checkpoint_88000_eng_lj"
tacotron_kor_model_path = "/home/minds/maum/trained/tts/tacotron2/checkpoint_102500_cw"
wavenet_model_path = "/home/minds/maum/trained/tts/wavenet/wavenet_438000_eng_lj"

# create Model instances for each DNN model we are going to use
tacotron_kor_model = Model(name="tacotron_kor", model=Tacotron, kwargs={"checkpoint_path":tacotron_eng_model_path, "threshold": 0.5}, actor_list=[tacotron_kor])
tacotron_eng_model = Model(name="tacotron_eng", model=Tacotron, kwargs={"checkpoint_path":tacotron_eng_model_path, "threshold": 0.5}, actor_list=[tacotron_eng])
wavenet_model = Model(name="wavenet", model=WaveNet, kwargs={"model_filename":wavenet_model_path, "implementation": "many", "samples_per_read": 8000}, actor_list=[wavenet])
```
---

### `Class WorkerConfig`
represents a single instance of worker. Create for every instance of worker process you will spawn. `__init__` takes 3 mandatory positional arguements: `worker_id`, `model_list`, and `device`.

* `worker_id (int)` - Specify which worker this configuration is for. If there are 4 workers -- which you can specify with `dramatiq -p 4`-- workers will have `worker_id` from 0 ~ 3.
* `model_list (list)` - list of models the worker process will instantiate. **all WorkerConfig instances must have the SAME list of models** ( yes, this sounds unreasonable for now, and it will be patched soon )
* `device (int)` - all the models assigned to this worker will be loaded on GPU specified by `device`

```python
# configure each worker process to hold the models and respond to requests
# here, we are using 4 workers where worker 0 and 2 use GPU 0 and worker 1 and 3 use GPU 1
worker0 = WorkerConfig(worker_id = 0, model_list = [wavenet_model, tacotron_eng_model, tacotron_kor_model], device=0)
worker1 = WorkerConfig(worker_id = 1, model_list = [wavenet_model, tacotron_eng_model, tacotron_kor_model], device=1)
worker2 = WorkerConfig(worker_id = 2, model_list = [wavenet_model, tacotron_eng_model, tacotron_kor_model], device=0)
worker3 = WorkerConfig(worker_id = 3, model_list = [wavenet_model, tacotron_eng_model, tacotron_kor_model], device=1)
```
---

### `Class WorkerMiddleware` 
instantiate once for the entire server. Takes a single positional arguement: `worker_list`. After you instantiate an instance, must add the middleware to broker with `broker.add_middleware(your_worker_middleware)`.

* `worker_list (list)` - a list of `WorkerConfig` instances.

```python
# add all the configurations to dramatiq
broker.add_middleware(WorkerMiddleware([worker0, worker1, worker2, worker3]))
```

complete example can be found in [tts_server.py](https://github.com/mindslab-ai/graphiqal/blob/master/examples/tts_example/tts_server.py)
