# Throughput benchmark code guide

we use [pytest-benchmark](https://pypi.org/project/pytest-benchmark/) benchmarking framework. pytest-benchmark performs all tests in serial.

## File naming convention

1. benchmarkig code **MUST be in its own file**
1. file name **MUST start with `benchmark`** (ex. `benchmark_tacotron.py`)

## Writing benchmark code

> **MUST have at least 3 different benchmarking functions**

> **MUST use the same test cases ( same input & output ) used in test code**

> **DO NOT call any function or write any code, other than `import` and your benchmark function**

1. test function **MUST start with name `test`**
1. test function **MUST take a single argument -- `benchmark`** that will be automatically passed in my `pytest-benchmark`
1. initialize class
1. give the method & inputs as arguments to `benchmark`

## `benchmark.pedantic()`

Configure `iterations` and `rounds` to benchmark your engine with low deviation.

Keyword arguments: [pytest-benchmark 2.5 doc](https://pytest-benchmark.readthedocs.io/en/v2.5.0/readme.html)
* `kwargs (dict)`: dictionary of keyword arguments (ex. to pass arguments to `Tacotron.GetMel(text, speaker)`, we pass in dictionary `{"text": text, "speaker": speaker_id}`)
* `iterations (int)`: a single execution of the defined test / benchmark function. If the standard deviation is high, we recommend arounds around 5 ~ 10 depending on how slow your algorithm is.
* `rounds (int)`: a set of iterations. number of rounds to run. **Stats are computed with rounds, not with iterations.** We recommend small rounds, around 1 ~ 3 rounds.

```python
# content of "benchmark_tacotron.py"

From tacotron_class import Tacotron

# initialize model
tacotorn = Tacotron(arg)

############### test case 1 ###############
# takes `benchmark` as the only argument
def test_short_text(benchmark):
    text = "hello world"
    speaker_id = 0
    
    # model 
    benchmark.pednatic(tacotron.GetMel, kwargs={"text": text, "speaker": speaker_id}, iterations = 10, rounds = 2)
    
    
############### test case 2 ###############
def test_long_text(benchmark):
    text = "this is a really really long text because we have to test for various inputs with different\
            characteritics in order to thoroughly test for all edge cases."
    speaker_id = 0
    
    # model 
    benchmark.pednatic(tacotron.GetMel, kwargs={"text": text, "speaker": speaker_id}, iterations = 10, rounds = 2)

############### test case 3 ###############
def test_long_text_different_speaker(benchmark):
    text = "this is a really really long text because we have to test for various inputs with different\
            characteritics in order to thoroughly test for all edge cases."
    speaker_id = 1
    
    # model 
    benchmark.pednatic(tacotron.GetMel, kwargs={"text": text, "speaker": speaker_id}, iterations = 10, rounds = 2)
```

## Running benchmark

> check that benchmark runs correctly before submitting

1. `pip install pytest`
1. `pip install pytest-benchmark`
1. go to directory with your benchmark code
1. in terminal, run: `pytest  --benchmark-only`
