# Before you get started...

## How it works
![graphiqal diagram](https://github.com/mindslab-ai/graphiqal/blob/master/docs/graphiqal_diagram.jpg)

1. Requests are enqueued in RabbitMQ as JSON objects
2. graphiqal dequeues requests from RabbitMQ
3. A worker process processes the request
4. Result is stored in Redis, ready to be read

### Some imoportant details for server developers:
* RabbitMQ, Redis, and server can and should reside on different nodes
* there can be any number of server nodes listening to the same node of RabbitMQ and Redis
* client can be built with any language that are supported by both RabbitMQ and Redis (ex. Python, Java, C, C++, Go, Node.js, Lua, PHP, Swift, Objective-C, Java Spring Framework, Rust, Scalan, and more ) [RabbitMQ supported languages](https://www.rabbitmq.com/devtools.html), [Redis supported languages](https://redis.io/clients)

## Requirements
1. RabbitMQ ( [installation guide for CentOS7](https://www.howtoforge.com/tutorial/how-to-install-rabbitmq-server-on-centos-7/))
2. Redis 5.0 or greater ( [installation guide for CentOS7](https://linuxize.com/post/how-to-install-and-configure-redis-on-centos-7/) )
3. Python 3.5 or greater

## Installation

1. install RabbitMQ ( [installation guide for CentOS7](https://www.howtoforge.com/tutorial/how-to-install-rabbitmq-server-on-centos-7/))
1. install Redis 5.0 or greater ( [installation guide for CentOS7](https://linuxize.com/post/how-to-install-and-configure-redis-on-centos-7/) )
1. create Python virtual envrionment
1. clone the repo
1. `cd graphiqal`
1. `sudo python -m pip install '.[all]'` ( for graphiqal developers, use: `sudo python -m pip install -e '.[all]'` )
---

# For DNN algorithm developers

## 1. style guide
[style guide](https://github.com/mindslab-ai/graphiqal/blob/master/docs/developer_gudie.md)

## 2. how to wrap your model in Class
[wrapping model guide](https://github.com/mindslab-ai/graphiqal/blob/master/docs/wrapping_model_guide.md)

## 3. write test code
[how to write test code](https://github.com/mindslab-ai/graphiqal/blob/master/docs/test_code_guide.md)

## 4. write throughput benchmark code
[how to write benchmark code](https://github.com/mindslab-ai/graphiqal/blob/master/docs/benchmark_code_guide.md)

---

# For server developers

## 1. how to write actors
[actor guide](https://github.com/mindslab-ai/graphiqal/blob/master/docs/actor_guide.md)

## 2. how to create server
[server guide](https://github.com/mindslab-ai/graphiqal/blob/master/docs/server_guide.md)

## 3. how to create client
[client guide](https://github.com/mindslab-ai/graphiqal/blob/master/docs/request_guide.md)
