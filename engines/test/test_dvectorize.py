import sys

sys.path.insert(0, "/home/msl/gom/graphiqal/engines")
from dvectorize import dvectorize_client

wave1 = "moon1.wav"
wave2 = "moon2.wav"
wave3 = "park1.wav"
wave4 = "park2.wav"

dvec1 = "moon1.dvec"
dvec2 = "moon2.dvec"
dvec3 = "park1.dvec"
dvec4 = "park2.dvec"

def test_dvectorize1() :
    assert dvectorize_client.dvectorize_action(wave1, dvec1) == True


def test_dvectorize2() :
    assert dvectorize_client.dvectorize_action(wave2, dvec2) == True


def test_dvectorize3() :
    assert dvectorize_client.dvectorize_action(wave3, dvec3) == True


def test_dvectorize4() :
    assert dvectorize_client.dvectorize_action(wave4, dvec4) == True


def test_dvectorize5() :
    assert dvectorize_client.dvectorize_action(wave1, dvec2) == False


def test_dvectorize6() :
    assert dvectorize_client.dvectorize_action(wave1, dvec3) == False


def test_dvectorize7() :
    assert dvectorize_client.dvectorize_action(wave1, dvec4) == False


def test_dvectorize8() :
    assert dvectorize_client.dvectorize_action(wave2, dvec1) == False
