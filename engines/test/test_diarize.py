import sys
import torch
import os
import time

sys.path.insert(0, "/home/msl/gom/graphiqal/engines")
from diarize import diarize_client

pt1 = "dambo-short.pt"
ref_output = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'diarize', "dambo-short.diarize"))

def test_diarize1() :
    start = time.time()
    result_output = diarize_client.diarize_action(pt1)
    end = time.time()
    print (str(end - start))
    assert ref_output == result_output

def test_diarize2() :
    start = time.time()
    result_output = diarize_client.diarize_action(pt1)
    end = time.time()
    print (str(end - start))
    assert ref_output == result_output

def test_diarize3() :
    start = time.time()
    result_output = diarize_client.diarize_action(pt1)
    end = time.time()
    print (str(end - start))
    assert ref_output == result_output

def test_diarize4() :
    start = time.time()
    result_output = diarize_client.diarize_action(pt1)
    end = time.time()
    print (str(end - start))
    assert ref_output == result_output

def test_diarize5() :
    start = time.time()
    result_output = diarize_client.diarize_action(pt1)
    end = time.time()
    print (str(end - start))
    assert ref_output == result_output

def test_diarize6() :
    start = time.time()
    result_output = diarize_client.diarize_action(pt1)
    end = time.time()
    print (str(end - start))
    assert ref_output == result_output

# test_diarize1()
# test_diarize2()
# test_diarize3()
# test_diarize4()
# test_diarize5()
# test_diarize6()