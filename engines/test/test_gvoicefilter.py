import sys
import os
import time
import numpy as np

sys.path.insert(0, "/home/msl/gom/graphiqal/engines")
from gvoicefilter import gvoicefilter_client

sys.path.insert(0, '/home/msl/gom/brain-dap/pysrc/maum/brain/dap/gvoicefilter')
from utils.utils import read_wav

wave1 = "simahn-cut-16k.wav"
gvlad1 = "sim-sample.gvlad"
gvlad2 = "ahn-sample.gvlad"

sr, ref_wav_output1 = read_wav(
    os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'gvoicefilter', 'sim-result.wav'))
sr, ref_wav_output2 = read_wav(
    os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'gvoicefilter', 'ahn-result.wav'))

def compare_list(x, y):
    assert np.max(np.abs(np.array(x) - np.array(y))) < 1e-6
    return True

def test_gvoicefilter1() :
    start = time.time()
    result_output = gvoicefilter_client.gvoicefilter_action(wave1, gvlad1)
    end = time.time()
    print (str(end - start))
    assert compare_list(ref_wav_output1, result_output) == True

def test_gvoicefilter2() :
    start = time.time()
    result_output = gvoicefilter_client.gvoicefilter_action(wave1, gvlad2)
    end = time.time()
    print (str(end - start))
    assert compare_list(ref_wav_output2, result_output) == True

test_gvoicefilter1()
test_gvoicefilter2()