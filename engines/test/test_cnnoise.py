import sys
import os
import time
import numpy as np

sys.path.insert(0, "/home/msl/gom/graphiqal/engines")
from cnnoise import cnnoise_client

sys.path.insert(0, '/home/msl/swpark/brain-dap/pysrc/maum/brain/dap/cnnoise')
from utils.utils import read_wav

wave1 = "noisy.wav"
wave2 = "curry-16k-cut.wav"
wave3 = "moon2-cut.wav"
wave4 = "msl-seo-16k-cut.wav"

sr, ref_output1 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'noisy-denoised.wav'))
sr, ref_output2 = read_wav(
    os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'curry-16k-cut-denoised.wav'))
sr, ref_output3 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'moon2-cut-denoised.wav'))
sr, ref_output4 = read_wav(
    os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'msl-seo-16k-cut-denoised.wav'))

def compare_list(x, y):
    assert np.max(np.abs(np.array(x) - np.array(y))) < 1e-6
    return True

def test_cnnoise1() :
    start = time.time()
    result_output = cnnoise_client.cnnoice_action(wave1)
    end = time.time()
    print (str(end - start))
    assert compare_list(ref_output1, result_output) == True

def test_cnnoise2() :
    start = time.time()
    result_output = cnnoise_client.cnnoice_action(wave2)
    end = time.time()
    print (str(end - start))
    assert compare_list(ref_output2, result_output) == True

def test_cnnoise3() :
    start = time.time()
    result_output = cnnoise_client.cnnoice_action(wave3)
    end = time.time()
    print (str(end - start))
    assert compare_list(ref_output3, result_output) == True

def test_cnnoise4() :
    start = time.time()
    result_output = cnnoise_client.cnnoice_action(wave4)
    end = time.time()
    print (str(end - start))
    assert compare_list(ref_output4, result_output) == True

test_cnnoise1()
test_cnnoise2()
test_cnnoise3()
test_cnnoise4()