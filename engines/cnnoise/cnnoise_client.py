import sys
import os
sys.path.insert(0, "/home/msl/gom/graphiqal/engines")
from cnnoise.cnnoise_server import cnnoise_actor

sys.path.insert(0, '/home/msl/gom/brain-dap/pysrc/maum/brain/dap/cnnoise')
from utils.utils import read_wav

wave1 = "noisy.wav"
wave2 = "curry-16k-cut.wav"
wave3 = "moon2-cut.wav"
wave4 = "msl-seo-16k-cut.wav"

def cnnoice_action(wave):
    sr, wave_list = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', wave))
    cnnoise_result = cnnoise_actor.send(wave_list)
    result_data = cnnoise_result.get_result(block=True, timeout=10000)
    return result_data

cnnoice_action(wave1)