import dramatiq
import sys

@dramatiq.actor(store_results=True)
def gpt_actor(context, *, model):
    out = model.infer(context)
    return out
