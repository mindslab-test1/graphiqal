######################################## COPY & PASTE ########################################
# necessary imports for the server
import dramatiq
import sys
import time
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from dramatiq.results.backends.redis import RedisBackend
from dramatiq.results import Results
from dramatiq.middleware.worker_middleware import WorkerMiddleware, Model, WorkerConfig

# setup server
broker = RabbitmqBroker()
redis_client = RedisBackend(password="msl1234~")
broker.add_middleware(Results(backend=redis_client))
dramatiq.set_broker(broker)
##############################################################################################

sys.path.insert(0, "/home/msl/gom/graphiqal/engines")
from gpt.gpt_actor import gpt_actor
sys.path.insert(0, '/home/msl/gom/brain-lm/pysrc')
from maum.brain.gpt.gpt_news.run.gpt_inference import GPT

gpt_model_path = "/home/msl/gom/brain-lm/cnn_category_1024"

gpt_model = Model(name="bert_mrc", model=GPT, kwargs={'model_path':gpt_model_path, 'conditional':True, 'seed':0, 'nsamples':1, 'batch_size':1, 'length':None, 'temperature':1, "top_k":0, "top_p":0.0}, actor_list=[gpt_actor])

worker0 = WorkerConfig(worker_id = 0, model_list = [gpt_model], device=0)
worker1 = WorkerConfig(worker_id = 1, model_list = [gpt_model], device=1)
worker2 = WorkerConfig(worker_id = 2, model_list = [gpt_model], device=0)
worker3 = WorkerConfig(worker_id = 3, model_list = [gpt_model], device=1)

# broker.add_middleware(WorkerMiddleware([worker0, worker1]))
broker.add_middleware(WorkerMiddleware([worker0, worker1, worker2, worker3]))