import sys
import time
sys.path.insert(0, "/home/msl/gom/graphiqal/engines")
from gpt.gpt_server import gpt_actor

input1 = "As reported by ESPN's Michelle Beadle on Around the NFL on Sunday,"
input2 = "Like China, which is locked in a full-blown trade war with the United States,"
input3 = "But it doesn't have the same firepower as neighboring China, the world's second largest economy,"

def gpt_action(context) :
    gpt_result = gpt_actor.send(context)
    result_data = gpt_result.get_result(block=True, timeout=120000)
    print(result_data)
    return result_data
