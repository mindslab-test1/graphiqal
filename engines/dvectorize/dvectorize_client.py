import sys
sys.path.insert(0, "/home/msl/gom/graphiqal/engines")
from dvectorize.dvectorize_server import dvectorize_actor
import torch
import os
import time

sys.path.insert(0, '/home/msl/gom/brain-dap/pysrc/maum/brain/dap/dvectorize')
from core.utils import read_wav

wave1 = "moon1.wav"
wave2 = "moon2.wav"
wave3 = "park1.wav"
wave4 = "park2.wav"

dvec1 = "moon1.dvec"
dvec2 = "moon2.dvec"
dvec3 = "park1.dvec"
dvec4 = "park2.dvec"

def dvectorize_action(wave, dvec) :
    sr, wav_list = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'dvectorize', wave))
    voice_vector = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'dvectorize', dvec))
    dvectorize_result = dvectorize_actor.send(wav_list, voice_vector)
    result_data = dvectorize_result.get_result(block=True, timeout=10000)

    # print(result_data)
    return result_data

# start = time.time()
# dvectorize_action(wave1, dvec1)
# end = time.time()
# print("takenTime : " + str(end-start))
# 
# start = time.time()
# dvectorize_action(wave2, dvec2)
# end = time.time()
# print("takenTime : " + str(end-start))
# 
# start = time.time()
# dvectorize_action(wave3, dvec3)
# end = time.time()
# print("takenTime : " + str(end-start))
# 
# start = time.time()
# dvectorize_action(wave4, dvec4)
# end = time.time()
# print("takenTime : " + str(end-start))
# 
# start = time.time()
# dvectorize_action(wave3, dvec1)
# end = time.time()
# print("takenTime : " + str(end-start))
# 
# start = time.time()
# dvectorize_action(wave3, dvec2)
# end = time.time()
# print("takenTime : " + str(end-start))
# 
# start = time.time()
# dvectorize_action(wave3, dvec3)
# end = time.time()
# print("takenTime : " + str(end-start))
# 
# start = time.time()
# dvectorize_action(wave3, dvec4)
# end = time.time()
# print("takenTime : " + str(end-start))
# 
# start = time.time()
# dvectorize_action(wave2, dvec1)
# end = time.time()
# print("takenTime : " + str(end-start))
