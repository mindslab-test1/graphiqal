import dramatiq
import numpy as np

@dramatiq.actor(store_results=True)
def dvectorize_actor(wav_list, voice_vector, *, model):

    result_output = model.GetDvectorFromWav(wav_list, True)

    try:
        compare_list(voice_vector, result_output)
        return True
    except Exception :
        return False

def compare_list(x, y):
    assert np.max(np.abs(np.array(x) - np.array(y))) < 1e-6