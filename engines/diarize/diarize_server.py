######################################## COPY & PASTE ########################################
# necessary imports for the server
import dramatiq
import sys
import os
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from dramatiq.results.backends.redis import RedisBackend
from dramatiq.results import Results
from dramatiq.middleware.worker_middleware import WorkerMiddleware, Model, WorkerConfig

# setup server
broker = RabbitmqBroker()
redis_client = RedisBackend(password="msl1234~")
broker.add_middleware(Results(backend=redis_client))
dramatiq.set_broker(broker)
##############################################################################################

sys.path.insert(0, '/home/msl/gom/brain-dap/pysrc')
from maum.brain.dap.diarize.run.diarize_graphiqal import Diarize
sys.path.insert(0, "/home/msl/gom/graphiqal/engines")
from diarize.diarize_actor import diarize_actor

diarize_model = Model(name="diarize", model=Diarize, kwargs={'checkpoint_path':os.path.join(os.environ['MAUM_ROOT'], 'trained', 'dap', 'diarize', 'diarization_v0.pt') }, actor_list=[diarize_actor])

worker0 = WorkerConfig(worker_id = 0, model_list = [diarize_model], device=0)
worker1 = WorkerConfig(worker_id = 1, model_list = [diarize_model], device=1)
worker2 = WorkerConfig(worker_id = 2, model_list = [diarize_model], device=0)
worker3 = WorkerConfig(worker_id = 3, model_list = [diarize_model], device=1)

broker.add_middleware(WorkerMiddleware([worker0, worker1, worker2, worker3]))
