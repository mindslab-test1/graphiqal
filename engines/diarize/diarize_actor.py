import dramatiq

@dramatiq.actor(store_results=True)
def diarize_actor(input_array, *, model):

    result_output = model.GetDiarization(input_array)
    return result_output