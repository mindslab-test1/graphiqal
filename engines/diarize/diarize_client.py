import sys
sys.path.insert(0, "/home/msl/gom/graphiqal/engines")
from diarize.diarize_server import diarize_actor
import torch
import os

pt1 = "dambo-short.pt"
diarize1 = "dambo-short.diarize"

def diarize_action(pt):
    ref_input = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'diarize', pt))
    diarize_reault = diarize_actor.send(ref_input)
    result_data = diarize_reault.get_result(block=True, timeout=10000)
    return result_data

diarize_action(pt1)