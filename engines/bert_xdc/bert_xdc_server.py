######################################## COPY & PASTE ########################################
# necessary imports for the server
import dramatiq
import sys
import os
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from dramatiq.results.backends.redis import RedisBackend
from dramatiq.results import Results
from dramatiq.middleware.worker_middleware import WorkerMiddleware, Model, WorkerConfig

# setup server
broker = RabbitmqBroker()
redis_client = RedisBackend(password="msl1234~")
broker.add_middleware(Results(backend=redis_client))
dramatiq.set_broker(broker)
##############################################################################################

sys.path.insert(0, '/home/msl/gom/brain-lm/pysrc')
from maum.brain.bert.xdc.run.xdc_inference import BertXDC
sys.path.insert(0, "/home/msl/gom/graphiqal/engines")
from bert_xdc.bert_xdc_actor import bert_xdc_actor
import json

config = json.load(open(os.environ['MAUM_ROOT']+'/trained/bert/xdc_config.json', 'r'))

bert_xdc_model = Model(name="bert_mrc", model=BertXDC, kwargs={'config':config, 'lang':'ko'}, actor_list=[bert_xdc_actor])

worker0 = WorkerConfig(worker_id = 0, model_list = [bert_xdc_model], device=0)
worker1 = WorkerConfig(worker_id = 1, model_list = [bert_xdc_model], device=1)
worker2 = WorkerConfig(worker_id = 2, model_list = [bert_xdc_model], device=0)
worker3 = WorkerConfig(worker_id = 3, model_list = [bert_xdc_model], device=1)

broker.add_middleware(WorkerMiddleware([worker0, worker1, worker2, worker3]))