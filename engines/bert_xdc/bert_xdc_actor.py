import dramatiq

@dramatiq.actor(store_results=True)
def bert_xdc_actor(context, *, model):
    out = model.infer(context)
    return out
