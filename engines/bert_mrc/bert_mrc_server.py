######################################## COPY & PASTE ########################################
# necessary imports for the server
import dramatiq
import sys
import time
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from dramatiq.results.backends.redis import RedisBackend
from dramatiq.results import Results
from dramatiq.middleware.worker_middleware import WorkerMiddleware, Model, WorkerConfig

# setup server
broker = RabbitmqBroker()
redis_client = RedisBackend(password="msl1234~")
broker.add_middleware(Results(backend=redis_client))
dramatiq.set_broker(broker)
##############################################################################################

sys.path.insert(0, '/home/msl/isaac/brain-bert/pysrc')
from maum.brain.bert.mrc.run.inference import BertSquad
from bert_mrc_actor import bert_mrc_actor
import json

bert_mrc_model_path = "/trained/bert/bert-eng/model.ckpt-7300"

bert_mrc_model = Model(name="bert_mrc", model=BertSquad, kwargs={'config_path':'/home/msl/maum/trained/bert/config.json', 'lang':'ko', 'init_checkpoint':'/trained/bert/bert-kor/model.ckpt-15386'}, actor_list=[bert_mrc_actor])

worker0 = WorkerConfig(worker_id = 0, model_list = [bert_mrc_model], device=1)
worker1 = WorkerConfig(worker_id = 1, model_list = [bert_mrc_model], device=1)
worker2 = WorkerConfig(worker_id = 2, model_list = [bert_mrc_model], device=0)
worker3 = WorkerConfig(worker_id = 3, model_list = [bert_mrc_model], device=1)

broker.add_middleware(WorkerMiddleware([worker0, worker1, worker2, worker3]))