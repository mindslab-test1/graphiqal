import dramatiq
import sys

sys.path.insert(0, '/home/msl/isaac/brain-bert/pysrc/maum/brain/bert/mrc/')
from core.transform_data_for_bert import transform_data

@dramatiq.actor(store_results=True)
def bert_mrc_actor(context, question, *, model):
    json_input = transform_data(context, question)
    answer = model.infer(json_input)
    return answer

