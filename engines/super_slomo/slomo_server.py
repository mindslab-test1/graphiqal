import sys

import dramatiq

sys.path.insert(0,"/home/msl/changho/graphiqa/dramatiq/")
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from dramatiq.results.backends.redis import RedisBackend
from dramatiq.results import Results
from dramatiq.middleware.worker_middleware import WorkerMiddleware, Model, WorkerConfig

sys.path.insert(0, '/home/msl/changho/graphiqal/dramatiq/server_code')
from SuperSloMo import SuperSloMo
from slomo_actor import slomo_actor

broker = RabbitmqBroker()
redis_client = RedisBackend(password="msl1234~")
broker.add_middleware(Results(backend=redis_client))
dramatiq.set_broker(broker)

slomo_model_path = "/home/msl/changho/graphiqal/dramatiq/server_code/SuperSloMo.ckpt"

slomo_model = Model(name="slomo", model=SuperSloMo, kwargs={"checkpoint": slomo_model_path}, actor_list=[slomo_actor])

worker0 = WorkerConfig(worker_id = 0, model_list = [slomo_model], device=0)
worker1 = WorkerConfig(worker_id = 1, model_list = [slomo_model], device=0)
worker2 = WorkerConfig(worker_id = 2, model_list = [slomo_model], device=1)
worker3 = WorkerConfig(worker_id = 3, model_list = [slomo_model], device=1)

broker.add_middleware(WorkerMiddleware([worker0, worker1, worker2, worker3]))