import dramatiq

# @dramatiq.actor()
@dramatiq.actor(store_results=True)
def slomo_actor(batch, w, h, fps_scale, *, model):
    output = model.GetSloMo(batch, w, h, fps_scale)
    return output
