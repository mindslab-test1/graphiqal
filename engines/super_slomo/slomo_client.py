import sys
import cv2
import torch
from PIL import Image
from torchvision import transforms
import numpy as np

trans_forward = transforms.ToTensor()
trans_backward = transforms.ToPILImage()
mean = [0.429, 0.431, 0.397]
mea0 = [-m for m in mean]
std = [1] * 3
trans_forward = transforms.Compose([trans_forward, transforms.Normalize(mean=mean, std=std)])
trans_backward = transforms.Compose([transforms.Normalize(mean=mea0, std=std), trans_backward])

def load_batch(video_in, batch_size, batch, w, h):
    if len(batch) > 0:
        batch = [batch[-1]]

    for i in range(batch_size):
        ok, frame = video_in.read()
        if not ok:
            break
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = Image.fromarray(frame)  # float to int
        frame = frame.resize((w, h), Image.ANTIALIAS)
        frame = frame.convert('RGB')
        frame = trans_forward(frame)
        batch.append(frame)

    return batch


def denorm_frame(frame, w0, h0):
    # frame = frame.cpu()
    frame = trans_backward(frame)
    frame = frame.resize((w0, h0), Image.BILINEAR)
    frame = frame.convert('RGB')
    return np.array(frame)[:, :, ::-1].copy()


from slomo_server import slomo_actor

if __name__ == "__main__":

    input_path = "/home/msl/gom/graphiqal/engines/super_slomo/input.mp4"
    output_path = "/home/msl/gom/graphiqal/engines/super_slomo/output.mp4"
    fps_scale = 4

    batch_size = 2

    cam = cv2.VideoCapture(input_path)

    input_fps = cam.get(5)
    fr = int(input_fps) + (1 if (input_fps - int(input_fps) >= 0.5) else 0)
    output_fps = fr

    output_format = 'mp4v'

    vin = cv2.VideoCapture(input_path)
    count = vin.get(cv2.CAP_PROP_FRAME_COUNT)
    w0, h0 = int(vin.get(cv2.CAP_PROP_FRAME_WIDTH)), int(vin.get(cv2.CAP_PROP_FRAME_HEIGHT))
    codec = cv2.VideoWriter_fourcc(*output_format)
    vout = cv2.VideoWriter(output_path, codec, float(output_fps), (w0, h0))

    w, h = 32*(w0//32), 32*(h0//32)

    print("slomo_request_start!")

    frame = []
    batch = []
    while True:
        batch = load_batch(vin, batch_size, batch, w, h)
        if len(batch) == 1:
            break

        x = []
        for t in batch:
            tt = np.array(t)
            tt = tt.tolist()
            x.append(tt)

        frame.append(x)

    print("batch seperaterated")
    print(len(frame))

    result_list = []
    for i in range(len(frame)):
        # if i ==0:
        #     print("size of input")
        #     x = sys.getsizeof(frame[i])
        #     print(x)
        result_list.append(slomo_actor.send(frame[i], w, h, fps_scale))

    print("get result")

    for i in range(len(frame)):
        video = result_list[i].get_result(block=True, timeout=6000000)
        # if i ==0:
        #     print("size of output")
        #     x = sys.getsizeof(video)
        #     print(x)
        for j in range(len(video)):
            t = []
            for k in range(len(video[j])):
                x = np.array(video[j][k])
                x = torch.FloatTensor(x)
                t.append(x)

            video[j] = t

        for fid, iframe in enumerate(video):
            fr = np.array(frame[i][fid])
            fr = torch.FloatTensor(fr)
            vout.write(denorm_frame(fr, w0, h0))

            for frm in iframe:
                vout.write(denorm_frame(frm, w0, h0))

        print("progress: ",i)

    vout.write(denorm_frame(batch[0], w0, h0))

    print("slomo_request done")

    vin.release()
    vout.release()