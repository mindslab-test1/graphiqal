import dramatiq
import sys
import base64
sys.path.insert(0, '/home/msl/gom/brain-dap/pysrc')
from maum.brain.dap.ghostvlad.core.utils import read_wav


@dramatiq.actor(store_results=True)
def ghostvlad_actor(wav_str, *, model):

    wav_decode = base64.b64decode(wav_str)

    file = open("temp.wav", 'wb')
    file.write(wav_decode)
    file.close()

    sr, wav_list = read_wav("temp.wav")
    result_output = model.GetDvectorFromWav(wav_list)

    return result_output
