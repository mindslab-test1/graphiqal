from ghostvlad_server import ghostvlad_actor
import sys
import torch
import os
import time

sys.path.insert(0, '/home/msl/gom/brain-dap/pysrc/maum/brain/dap/ghostvlad')
from core.utils import read_wav

wave1 = "moon1.wav"
wave2 = "moon2.wav"
wave3 = "park1.wav"
wave4 = "park2.wav"

gvlad1 = "moon1.gvlad"
gvlad2 = "moon2.gvlad"
gvlad3 = "park1.gvlad"
gvlad4 = "park2.gvlad"

def ghostvlad_action(wave, gvlad) :
    sr, wav_list = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'ghostvlad', wave))
    voice_vector = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'ghostvlad', gvlad)).tolist()
    ghostvlad_result = ghostvlad_actor.send(wav_list, voice_vector)

    result_data = ghostvlad_result.get_result(block=True, timeout=10000)

    print(result_data)

start = time.time()
ghostvlad_action(wave1, gvlad1)
end = time.time()
print("takenTime : " + str(end-start))

start = time.time()
ghostvlad_action(wave2, gvlad2)
end = time.time()
print("takenTime : " + str(end-start))

start = time.time()
ghostvlad_action(wave3, gvlad3)
end = time.time()
print("takenTime : " + str(end-start))

start = time.time()
ghostvlad_action(wave4, gvlad4)
end = time.time()
print("takenTime : " + str(end-start))

start = time.time()
ghostvlad_action(wave3, gvlad1)
end = time.time()
print("takenTime : " + str(end-start))

start = time.time()
ghostvlad_action(wave3, gvlad2)
end = time.time()
print("takenTime : " + str(end-start))

start = time.time()
ghostvlad_action(wave3, gvlad3)
end = time.time()
print("takenTime : " + str(end-start))

start = time.time()
ghostvlad_action(wave3, gvlad4)
end = time.time()
print("takenTime : " + str(end-start))

start = time.time()
ghostvlad_action(wave2, gvlad1)
end = time.time()
print("takenTime : " + str(end-start))
