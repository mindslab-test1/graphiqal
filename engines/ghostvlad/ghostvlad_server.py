######################################## COPY & PASTE ########################################
# necessary imports for the server
import dramatiq
import sys
import os
import time
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from dramatiq.results.backends.redis import RedisBackend
from dramatiq.results import Results
from dramatiq.middleware.worker_middleware import WorkerMiddleware, Model, WorkerConfig

# setup server
broker = RabbitmqBroker()
redis_client = RedisBackend(password="msl1234~")
broker.add_middleware(Results(backend=redis_client))
dramatiq.set_broker(broker)
##############################################################################################

sys.path.insert(0, '/home/msl/gom/brain-dap/pysrc')
from maum.brain.dap.ghostvlad.run.ghostvlad_graphiqal import GhostVLAD
from ghostvlad_actor import ghostvlad_actor

ghostvlad_model = Model(name="ghostvlad", model=GhostVLAD, kwargs={'checkpoint_path':os.path.join(os.environ['MAUM_ROOT'], 'trained', 'dap', 'ghostvlad', 'nolrdecay_c07b57c_103.pt') }, actor_list=[ghostvlad_actor])

worker0 = WorkerConfig(worker_id = 0, model_list = [ghostvlad_model], device=1)
worker1 = WorkerConfig(worker_id = 1, model_list = [ghostvlad_model], device=1)
worker2 = WorkerConfig(worker_id = 2, model_list = [ghostvlad_model], device=0)
worker3 = WorkerConfig(worker_id = 3, model_list = [ghostvlad_model], device=1)

broker.add_middleware(WorkerMiddleware([worker0, worker1, worker2, worker3]))
