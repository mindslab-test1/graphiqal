import sys
import os
import torch
sys.path.insert(0, "/home/minds/gom/graphiqal/engines")
from gvoicefilter.gvoicefilter_server import gvoicefilter_actor

sys.path.insert(0, '/home/minds/gom/brain-dap/pysrc/maum/brain/dap/gvoicefilter')
from utils.utils import read_wav

wave1 = "simahn-cut-16k.wav"
gvlad1 = "sim-sample.gvlad"
gvlad2 = "ahn-sample.gvlad"

def gvoicefilter_action(wave, gvlad):
    sr, wave_list = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'gvoicefilter', wave))
    gvlad_list = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'gvoicefilter', gvlad)).tolist()
    sr_sample, sample_result = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'gvoicefilter', 'sim-result.wav'))
    gvoicefilter_result = gvoicefilter_actor.send(gvlad_list, wave_list)
    result_data = gvoicefilter_result.get_result(block=True, timeout=10000)
    assert result_data == sample_result

gvoicefilter_action(wave1, gvlad1)
