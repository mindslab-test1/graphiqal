import dramatiq
import base64
import sys
sys.path.insert(0, '/home/msl/gom/brain-dap/pysrc')
from maum.brain.dap.gvoicefilter.utils.utils import read_wav


@dramatiq.actor(store_results=True)
def gvoicefilter_actor(gvlad_list, wav_str, *, model):

    wav_decode = base64.b64decode(wav_str)

    file = open("temp.wav", 'wb')
    file.write(wav_decode)
    file.close()

    sr, wav_list = read_wav("temp.wav")
    result_output = model.GetFilteredFromVecWav(gvlad_list, wav_list)

    return result_output
