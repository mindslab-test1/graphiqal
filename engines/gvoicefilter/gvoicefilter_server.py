######################################## COPY & PASTE ########################################
# necessary imports for the server
import dramatiq
import sys
import os
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from dramatiq.results.backends.redis import RedisBackend
from dramatiq.results import Results
from dramatiq.middleware.worker_middleware import WorkerMiddleware, Model, WorkerConfig

# setup server
broker = RabbitmqBroker()
redis_client = RedisBackend(password="msl1234~")
broker.add_middleware(Results(backend=redis_client))
dramatiq.set_broker(broker)
##############################################################################################

sys.path.insert(0, '/home/msl/gom/brain-dap/pysrc')
from maum.brain.dap.gvoicefilter.gvoicefilter_graphiqal import GVoiceFilterServicer
sys.path.insert(0, "/home/msl/gom/graphiqal/engines")
from gvoicefilter.gvoicefilter_actor import gvoicefilter_actor

gvoicefilter_model = Model(name="gvoicefilter", model=GVoiceFilterServicer, kwargs={'checkpoint_path':os.path.join(os.environ['MAUM_ROOT'], 'trained', 'dap', 'gvoicefilter', 'minlevel60-chkpt_300.pt')}, actor_list=[gvoicefilter_actor])

worker0 = WorkerConfig(worker_id = 0, model_list = [gvoicefilter_model], device=0)
worker1 = WorkerConfig(worker_id = 1, model_list = [gvoicefilter_model], device=0)
worker2 = WorkerConfig(worker_id = 2, model_list = [gvoicefilter_model], device=0)
worker3 = WorkerConfig(worker_id = 3, model_list = [gvoicefilter_model], device=0)

broker.add_middleware(WorkerMiddleware([worker0, worker1, worker2, worker3]))