from ..logging import get_logger
from ..middleware import Middleware
from copy import deepcopy
from ..actor import Actor

class WorkerMiddleware(Middleware):
    """
        Note:
            * singe instance of WorkerMiddleware is created and saved in an instance of Broker

        Todo:
            * get rid of actor_model_dict
            * simply keep a dictionary of "model_name":model pairs, which
            will be passed on to every actor as an argument.
            * make dictionary read-only from the perspective of actors in order
            to prevent corruption of the dictinoary
    """
    def __init__(self, worker_config_list):
        self.worker_config_list = worker_config_list
        #self.worker_config_list= deepcopy(worker_config_list)
        assert(self.is_valid())

    def before_worker_boot(self, broker, worker):
        """If WorkerConfig instance is set up for the model:
        1. instantiates models per process
        2. save the models in Worker.actor_model_dict
        3. set up queues to listen to by adding actor's name to Worker.white_list

        Args:
            broker(Broker): a single instance of broker shared among all worker processes
            worker(Worker): an instance of worker process being instantiated

        Note:
            * this relies on Worker having actor_model_dict attribute
        """
        worker_config = self.get_worker_config(worker.worker_id)

        if worker_config is not None:

            # actor_model_dict keeps pairs of model name and the instance of loaded model
            for model in worker_config.model_list:
                kwargs = model.kwargs # Todo: use deepcopy or not?
                if worker_config.device != -1:
                    kwargs["device"] = worker_config.device
                instantiated_model = model.model(**kwargs)

                for actor_name in model.actor_name_set:
                    worker.actor_model_dict[actor_name] = instantiated_model

            # setup which queues this Worker will listen to
            for actor_name in worker_config.actor_name_set:
                worker.consumer_whitelist.add(actor_name)

        assert(self.is_valid())

    def get_worker_config(self, worker_id):
        """returns an instance of WorkerConfig, if it exists for the worker.
        Returns None otherwise.

        Parameters:
            worker_id(int): worker_id of the worker process

        Returns: WorkerConfig or None
        """
        for worker_config in self.worker_config_list:
            if worker_config.worker_id == worker_id:
                return worker_config
        return None

    def is_valid(self):
        if not isinstance(self.worker_config_list, list):
            return False
        return True

class Model:
    """wrapper around Pytorch / Tensorflow model.

    Args:
        **name (str): name of the model. Must be unique among the models that a
        worker will have. used as a key to dictinoary which saves all models
        with name:model paris.
        **model (custom class): Pytorch / Tensorflow class that defines how todo
        upload weight and perform inference
        **kwargs (dict): keyword arguments passed to __init__ of model
        **actor_list (list): list of actors that the model will be passed to as
        an argument

    Attributes:
        name(str): name of the model. MUST be unique to the model.
        model(Class): a class of a model. The default initializer of the class is used to instantiate the model.
        kwargs(dict): keyword arguments passed to the model on initialization
        actor_name_set(str set): set of strings, where each string is the name of an actor.
        Since the name of the actor is the same as the name of the queue for the actor, we add this name
        to Worker.white_list.

    Todo:
        * get rid of actor_list since we are going to let all the actors take
        a dictinoary with saved models as input ( s.t. all the actors will
        have full access to all the models )
        * check whether kwargs should be deep-copied or not
    """

    def __init__(self, *, name, model, kwargs={}, actor_list=[]):
        self.name = name
        self.model = model
        self.kwargs = kwargs

        # use set to eliminate possible duplicates
        self.actor_name_set = set()

        # add actors' names from actor_list to actor_name_set
        for actor in actor_list:
            # if it's an instance of Actor, only save the name to actor_name_set
            if isinstance(actor, Actor):
                self.actor_name_set.add(actor.actor_name)

            if isinstance(actor, str):
                self.actor_name_set.add(actor)

        assert (self.is_valid())

    def get_name(self):
        return self.name

    def is_valid(self):
        if not isinstance(self.kwargs, dict) or not isinstance(self.name, str) or not isinstance(self.actor_name_set, set) or self.model is None:
            return False

        for actor in self.actor_name_set:
            if not isinstance(actor, str):
                return False

        return True

class WorkerConfig:
    """Wrapper around each worker process that saves configuration of each
    worker.

    Attributes:
        **worker_id (int): unique ID for each worker in range [0, num_workers - 1].
        MUST be unique and in the legal range.
        **model_list (list): list of Model instances

    Todo:
        * get rid of actor_list and model_list since we are going to let all the
         actors take a dictinoary with saved models as input ( s.t. all the
         actors will have full access to all the models )
    """

    def __init__(self, *, worker_id, model_list=[], actor_list=[], device=-1):
            assert(isinstance(worker_id, int) and worker_id >= 0)
            assert(isinstance(model_list, list))
            assert(isinstance(actor_list, list))
            assert(isinstance(device, int))

            self.worker_id = worker_id
            self.model_list = model_list

            # aggregate all the actors' names from models and the input to 'actor_name_set'
            self.actor_name_set = set()
            for model in self.model_list:
                for actor_name in model.actor_name_set:
                    self.actor_name_set.add(actor_name)

            for actor in actor_list:
                if isinstance(actor, Actor):
                    self.actor_name_set.add(actor.actor_name)

                if isinstance(actor, str):
                    self.actor_name_set.add(actor)

            self.device = device
            assert (self.is_valid())

    def is_valid(self):
        if not isinstance(self.worker_id, int) or not isinstance(self.model_list, list) or not isinstance(self.device, int):
            return False

        if self.worker_id < 0:
            return False

        if self.device < -1:
            return False

        for actor_name in self.actor_name_set:
            if not isinstance(actor_name, str):
                return False

        # check if actor in each model is exclusive
        actor_set = set()
        num_actors = 0
        for model in self.model_list:
            for actor in model.actor_name_set:
                num_actors +=1
                actor_set.add(actor)

        if len(actor_set) != num_actors:
            return False

        return True