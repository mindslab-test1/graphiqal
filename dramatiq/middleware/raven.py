import dramatiq

class SentryMiddleware(dramatiq.Middleware):
    def __init__(self, raven_client):
        self.raven_client = raven_client

    def after_process_message(self, broker, message, *, result=None, exception=None):
        if exception is not None:
            self.raven_client.captureException()