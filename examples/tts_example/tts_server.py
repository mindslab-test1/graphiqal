######################################## COPY & PASTE ########################################
# necessary imports for the server
import dramatiq
import sys
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from dramatiq.results.backends.redis import RedisBackend
from dramatiq.results import Results
from dramatiq.middleware.worker_middleware import WorkerMiddleware, Model, WorkerConfig

# setup server
broker = RabbitmqBroker()
redis_client = RedisBackend()
broker.add_middleware(Results(backend=redis_client))
dramatiq.set_broker(broker)
##############################################################################################

#import actors
from tts_actor import tacotron_actor, wavenet_actor

# add path to search tree
sys.path.insert(0, '/home/minds/git/brain-tts/pysrc')

# import your DNN model class
from maum.brain.tts.tacotron2_old.run.tacotron_class import Tacotron
from maum.brain.tts.wavenet.run.wavenet_class import WaveNet

# create Model instances for each DNN model we are going to use
tacotron_eng_model_path = "/home/minds/maum/trained/tts/tacotron2/checkpoint_88000_eng_lj"
wavenet_model_path = "/home/minds/maum/trained/tts/wavenet/wavenet_438000_eng_lj"
tacotron_eng_model = Model(name="tacotron_eng", model=Tacotron, kwargs={"checkpoint_path":tacotron_eng_model_path, "threshold": 0.5}, actor_list=[tacotron_actor])
wavenet_model = Model(name="wavenet", model=WaveNet, kwargs={"model_filename":wavenet_model_path, "implementation": "many", "samples_per_read": 8000}, actor_list=[wavenet_actor])

# configure each worker process to hold the models and respond to requests
# here, we are using 4 workers
worker0 = WorkerConfig(worker_id = 0, model_list = [wavenet_model, tacotron_eng_model], device=0)
worker1 = WorkerConfig(worker_id = 1, model_list = [wavenet_model, tacotron_eng_model], device=1)
worker2 = WorkerConfig(worker_id = 2, model_list = [wavenet_model, tacotron_eng_model], device=0)
worker3 = WorkerConfig(worker_id = 3, model_list = [wavenet_model, tacotron_eng_model], device=1)

# add all the configurations to dramatiq
broker.add_middleware(WorkerMiddleware([worker0, worker1, worker2, worker3]))