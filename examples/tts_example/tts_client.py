from tts_server import tacotron_actor, wavenet_actor
import time
from dramatiq import pipeline
from scipy.io.wavfile import write
import numpy as np
import os

if __name__ == '__main__':
    num_run = 4

    # prepare directories to save the produced .wav files
    current_file_path = os.getcwd()
    wav_file_path = current_file_path + "/wav"
    if not os.path.exists(wav_file_path):
        os.makedirs(wav_file_path)

    for i in range(num_run):
        if not os.path.exists(wav_file_path + "/trump" + str(i)):
            os.makedirs(wav_file_path + "/trump" + str(i))

    start_time = time.time()
    pipe_results = []
    audios = []

    sentence_list = ["President Trump met with other leaders at the Group of 20 conference.",
                     "President Trump met with other leaders at the Group of 20 conference.",
                     "President Trump met with other leaders at the Group of 20 conference."]
    speaker = 0

    for _ in range(num_run):
        pipe_results.append(pipeline([tacotron_actor.message(sentence_list, speaker), wavenet_actor.message()], is_streaming=True).run())


    audio = []
    is_first = True
    for i in range(num_run):
        audio_piece_index = 0
        for piece in pipe_results[i].get_result(is_streaming=True, block=True, timeout=10000000):
            if is_first:
                is_first = not is_first
                audio_length = piece["sample_length"]
                print("length of audio: " + str(audio_length))
            audio_piece = piece["data"]
            write("{}/{}/{}.wav".format("wav", "trump" + str(i), "trump" + str(i) + "_" + str(audio_piece_index)), 16000, np.asarray(audio_piece).astype('int16'))
            audio_piece_index+=1

            audio.extend(audio_piece)
        audios.append(audio)
        audio = []

    audio_sec = len(audios[0]) * num_run/16000
    duration = time.time() - start_time
    print("total audio length = " + str(audio_sec))
    print("total time = " + str(duration))
    print("TPS: " + str(audio_sec/duration))

    for i in range(num_run):
        write("{}/{}.wav".format("wav", "trump"+str(i)), 16000, np.asarray(audios[i]).astype('int16'))