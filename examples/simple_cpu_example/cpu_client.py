from cpu_server import foo
import time

if __name__ == "__main__":
    num_run = 65
    start_time = time.time()

    result_list = []
    for _ in range(60):
        # send request to the server ( by enqueuing request to RabbitMQ )
        result_list.append(foo.send("hello world"))

    output_list = []
    for i in range(60):
        # wait 1 second for the result to be produced.
        output_list.append(result_list[i].get_result(block=True, timeout = 10000))

    duration = time.time() - start_time
    print("completed sending & recieving " + str(num_run) + " foo")
    print("total time taken = " + str(duration))
