from time import sleep
import dramatiq

@dramatiq.actor(time_limit=float("inf"), store_results=True)
def foo(string):
    print("foo called")

    # stall fro a little while to simulate real-world workload
    sleep(1)

    return string