import dramatiq
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from dramatiq.results.backends.redis import RedisBackend
from dramatiq.results import Results
from dramatiq.middleware.worker_middleware import WorkerConfig, WorkerMiddleware

broker = RabbitmqBroker()
redis_client = RedisBackend()
broker.add_middleware(Results(backend=redis_client))
dramatiq.set_broker(broker)

from cpu_actor import foo

num_processes = 32
workerConfig_list = []
for i in range(num_processes):
    workerConfig_list.append(WorkerConfig(worker_id=i, model_list=[], actor_list=[foo], device=-1))

broker.add_middleware(WorkerMiddleware(workerConfig_list))